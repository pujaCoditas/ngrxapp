
import {Action} from '@ngrx/store';


export enum movieActionTypes {
  search = '[Movie Component] search',
  Filter = '[Movie Component] filter',
  Sort = '[Movie Component] sort' 
}
export class ActionEx implements Action {
  readonly type;
  payload: any;
}
export class searchMovie implements ActionEx {
  readonly type = movieActionTypes.search
  constructor(public payload: any) {
  }
}
export class filterMovie implements ActionEx {
  readonly type = movieActionTypes.Filter
  constructor(public payload: any) {
  }
}
export class sortMovie implements ActionEx {
  readonly type = movieActionTypes.Sort
  constructor(public payload: any) {
  }
}