
import { ActionEx, movieActionTypes } from './movies.action';

export const initialState = [];

export function MovieReducer(state = initialState, action: ActionEx) {
  switch (action.type) {
    case movieActionTypes.search:
      return [action.payload];

    case movieActionTypes.Filter:
      return [action.payload];

    case movieActionTypes.Sort:
      return [action.payload];
    default:
      return state;
  }
}