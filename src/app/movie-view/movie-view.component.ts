import { Component, OnInit } from '@angular/core';
import { Movies } from '../movies';
import { Observable, from } from 'rxjs';
import { select, Store } from '@ngrx/store';
// import {MovieRemove} from '../movies.action';
import { filterMovie } from '../movies.action';
import { sortMovie } from '../movies.action';
import { searchMovie } from '../movies.action';
import { HttpClient } from '@angular/common/http';
import { element } from 'protractor';

@Component({
  selector: 'app-movie-view',
  templateUrl: './movie-view.component.html',
  styleUrls: ['./movie-view.component.css']
})
export class MovieViewComponent implements OnInit {

  movieData;
  movieInfo: Array<any> = [];
  fullMovie;
  movies;
  filteredData;
  p = 1;
  count = 4;
  datalength = 0;
  isFilterData = false;
  isData = false;

  constructor(private store: Store<{ movies: Movies[] }>, private http: HttpClient) {
    store.pipe(select('movies')).subscribe(data => {
      if (data[0]) {
        this.movies = data[0];
        this.isData = true;
        this.datalength = this.movies.data.length;
      }
    })
  }



  ngOnInit(): void {
  }

  searchMovie(searchText) {
    this.p = 1;
    this.http.get('http://www.omdbapi.com/?apikey=d19842a1&s=' + searchText)
      .subscribe(data => {
        this.movieData = data;
        this.movieInfo = this.movieData.Search;
        this.datalength = this.movieInfo.length;

        const movies = new Movies();
        this.movieInfo.forEach(data => {
          movies.data.push(data);
        })
        this.store.dispatch(new searchMovie(movies));
      })

  }

  filterMovie(event) {
    this.p = 1;
    const filterBy = event.target.value;
    const movies = new Movies();
    let data = [];

    if (filterBy == 0) {
      this.movies.data.forEach(data => {
        movies.data.push(data);
      })
      this.store.dispatch(new sortMovie(movies));
    }
    else if (filterBy == 1) {
      this.movieInfo.forEach(element => {
        if (element.Type == 'movie') {
          data.push(element);
        }
      });
      data.forEach(data => {
        movies.data.push(data);
      })
      this.store.dispatch(new sortMovie(movies));
    }
    else if (filterBy == 2) {
      this.movieInfo.forEach(element => {
        if (element.Type == 'series') {
          data.push(element);
        }
      });
      data.forEach(data => {
        movies.data.push(data);
      })
      this.store.dispatch(new sortMovie(movies));
    }
    else if (filterBy == 3) {
      this.movieInfo.forEach(element => {
        if (element.Type == 'game') {
          data.push(element);
        }
      });
      data.forEach(data => {
        movies.data.push(data);
      })
      this.store.dispatch(new sortMovie(movies));
    }

  }

  sortData(event) {
    const sortBy = event.target.value;
    const movies = new Movies();
    let data = [];
    data = this.movies.data;
    if (sortBy == 0) {
      data.forEach(data => {
        movies.data.push(data);
      })
      this.store.dispatch(new sortMovie(movies));
    }

    else if (sortBy == 1) {
      data = data.slice().sort(function (a, b) {
        var name1 = a.Title, name2 = b.Title;
        var nameA = name1.toLowerCase(), nameB = name2.toLowerCase()
        if (nameA < nameB) //sort string ascending
          return -1
        if (nameA > nameB)
          return 1
        return 0 //default return value (no sorting)
      })

      data.forEach(data => {
        movies.data.push(data);
      })
      this.store.dispatch(new sortMovie(movies));

    }

    else if (sortBy == 2) {
      data = data.slice().sort(function (a, b) {
        var name1 = a.Title, name2 = b.Title;
        var nameA = name1.toLowerCase(), nameB = name2.toLowerCase()
        if (nameA < nameB) //sort string descending
          return 1
        if (nameA > nameB)
          return -1
        return 0 //default return value (no sorting)
      })

      data.forEach(data => {
        movies.data.push(data);
      })
      this.store.dispatch(new sortMovie(movies));
    }

  }

}
